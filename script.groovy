def buildJar() {
    echo 'building the application...'
    sh 'mvn clean package'
}

def buildImage() {
    echo "building the docker image..."
    withCredentials([usernamePassword(credentialsId: 'docker-hub-repo', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        sh 'docker build -t kevinjia1984/demo-app:${IMAGE_NAME} .'
        sh 'echo $PASS | docker login -u $USER --password-stdin'
        sh 'docker push kevinjia1984/demo-app:${IMAGE_NAME}'
    }
}

def deployApp() {
    echo 'deploying the application...'
    def dockerCmd = 'docker run -p 8080:8080 -d ${IMAGE_NAME}'
    sshagent(['ec2-server-key']){
        sh "ssh -o StrictHostKeyChecking=no ec2-user@13.229.107.73 ${dockerCmd}"
    }

}

return this
